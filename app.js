var express = require("express");
var alexa = require("alexa-app");
var promise = require("promise");

const responses = require('./json/commonResponses.json');
const configuration = require('./json/configuration.json');
const subscribers = require('./json/subscriberdetail.json');

const CommonMethods = require('./lib/commonmethods.js');
const MainService = require('./lib/mainservice.js');
const MailHandler = require('./lib/googlemailhandler.js');


const APP_ID = configuration.appId;

var PORT = process.env.port || 8080;
var app = express();
var IsUserRecognized = false;

// ALWAYS setup the alexa app and attach it to express before anything else.
var alexaApp = new alexa.app("alexaWS");

alexaApp.express({
  expressApp: app,

  // verifies requests come from amazon alexa. Must be enabled for production.
  // You can disable this if you're running a dev environment and want to POST
  // things to test behavior. enabled by default.
  checkCert: false,

  // sets up a GET route when set to true. This is handy for testing in
  // development, but not recommended for production. disabled by default
  debug: true
});

// now POST calls to /test in express will be handled by the app.request() function

// from here on you can setup any other express routes or middlewares as normal
app.set("view engine", "ejs");


function initiateSession(request)
{

    // check if you can use session (read or write)
    if (request.hasSession())
    {
        // get the session object
        var session = request.getSession();
        var sessionDetail = session.details;
       
        var reqAppId = sessionDetail.application.applicationId;
        var alexaUserId = sessionDetail.user.userId;

        if (reqAppId != configuration.appId) {
            console.log('Invalid Alexa skill trying to call... ' + reqAppId);
            return null;
        }

        //session.set('userId',,alexaUserId);
        var currUserId = session.get('userId');

        console.log('user id identified as => ' + alexaUserId);
        console.log('current user => '+ currUserId);
   
        if (currUserId == null || currUserId == 'undefined' 
                || currUserId == '')
            {
                session.set('user','');
                session.set('user_email','');
                session.set('subs','');
                session.set('mercer_leadername','');
                session.set('intent_to_exec','');

                var user = CommonMethods.GetUserByUserId(alexaUserId,subscribers);
                
                if (user) {
                    session.set('userId',alexaUserId);
                    session.set('user',user.name);
                    session.set('user_email',user.email);
                    session.set('subs',user.name);
                    IsUserRecognized = true;
                }
            }
        }
}
              

function getCard(cardTitle,cardContent) {
    return {
        "type": "Simple",
        "title":cardTitle,
        "content": cardContent
    };
}

function responseToAlexa(response,responsetype,spkresponse,prompt,cardtitle,cardtext) {
       
        var cardObj = getCard(cardtitle,cardtext);
        console.log(cardObj);

        switch (responsetype.toUpperCase()) {
            case 'TELL': return response.say(spkresponse).send(); break;
            case 'ASK': return response.say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
            case 'ASKWITHCARD': return response.card(cardtitle,cardtext).say(spkresponse).reprompt(prompt).shouldEndSession(false).send();break;
        }
}
    
function intent_GetConfirmation(request,response) {
    var session = request.getSession();
    var useremail = session.get('user_email');
    if (useremail == null || useremail == '' || useremail == 'undefined') {
        useremail = 'arun.agrawal@mercer.com';
        session.set('user_email',useremail);
    }

    responseToAlexa(response,'ASK','I will send this information to ' + useremail + ', please confirm?','','','' );
}


alexaApp.error = function(exception, request, response) {
    console.log(exception);
    response.say("Sorry, something bad happened");
};

alexaApp.pre = function(request, response, type) {
    if (request.applicationId != configuration.appId) {
      // fail ungracefully
      //throw "Invalid applicationId";
      return response.fail("Invalid applicationId");
    }
  };

alexaApp.post = function(request, response, type, exception) {
    console.log('sending repsonse...');
    console.log(response);

    if (exception) {
      // always turn an exception into a successful response
      console.log('exception while sending response...');
      console.log(exception);
       return response.clear().say("An error occured: " + exception).send();
    }
  };  

alexaApp.launch(function(request, response) {
    responseToAlexa(response,'ASK',responses["LaunchRequest"].ask, responses["LaunchRequest"].reprompt,"","");
});

alexaApp.intent("AMAZON.HelpIntent", {},function (request, response) {
    responseToAlexa(response,'ASK',responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt,"","");
});

alexaApp.intent("AMAZON.StopIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileServiceStopped"].tell, "","","");
});

alexaApp.intent("AMAZON.CancelIntent", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileServiceStopped"].tell, "","","");
});

alexaApp.intent("SessionEndedRequest", {},function (request, response) {
    responseToAlexa(response,'TELL',responses["ProfileServiceStopped"].tell, "","","");
});

alexaApp.sessionEnded(function(request, response) {
    responseToAlexa(response,'TELL',responses["ProfileServiceStopped"].tell, "","","");
  });

alexaApp.intent("StartProfileService", {}, function(request, response) {
    responseToAlexa(response,'ASK', responses["ProfileServiceStarted"].ask, responses["ProfileServiceStarted"].reprompt,"","");
});

alexaApp.intent("StopProfileService", {}, function(request, response) {
    responseToAlexa(response,'TELL',responses["ProfileServiceStopped"].tell, "","","");
});

alexaApp.intent("GetConfirmation", {}, function(request, response) {
    intent_GetConfirmation(request, response);
});

alexaApp.intent("SetConfirmation", {}, function(request, response) {
    var cnfrmResponse = request.slot("CONSENT");
    var anotherAuthEmailId = request.slot("AUTHEMAIL");
    var session = request.getSession();
    var useremail = session.get('user_email');
    var subscriber = session.get('subs');
    var leadername = session.get('mercer_leadername');

    if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
            || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
    {
        console.log('mail confirmed to ' + useremail);

        if(session.get('intent_to_exec') == 'GetMeLeaderProfileSendTo') {
           
                    var intentAction = new promise(function(resolve,reject) {
                        
                                MainService.GetProfileSummary(response,leadername,MailHandler,subscriber, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                                    var outputObj = {
                                        responsetype: responsetype,
                                        spkresponse: spkresponse,
                                        reprompt: prompt,
                                        cardtitle: cardtitle,
                                        cardtext: cardtext
                                    };
                        
                                    resolve(outputObj);
                                });
                            });
                    
                    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });
                                 
            }   
            else {
                console.log('intent to exec is not matching');
            }
        }
        else {
            console.log('another email id');
            console.log(anotherAuthEmailId);
            if (anotherAuthEmailId) {
                    session.set('user_email',anotherAuthEmailId);
                    intent_GetConfirmation(request,response);
            }
            else {
                responseToAlexa(response,'ASK','okay','okay','','');
            }
        
        }
});

alexaApp.intent("GetMeLeaderProfile", {},  function(request, response) {

    var leadername = request.slot("LEADER");
    var session = request.getSession();

   
    initiateSession(request);
    session.set('mercer_leadername',leadername);
    //setSessionValues(this,leadername);
    
    var intentAction = new promise(function(resolve,reject) {

        MainService.GetProfileSummary(this,leadername,MailHandler,null, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
            var outputObj = {
                responsetype: responsetype,
                spkresponse: spkresponse,
                reprompt: prompt,
                cardtitle: cardtitle,
                cardtext: cardtext
            };

            resolve(outputObj);
        });
    });
    
    return intentAction.then(function(data){ responseToAlexa(response,data.responsetype,data.spkresponse,data.reprompt,data.cardtitle,data.cardtext); });

  });

alexaApp.intent("GetMeLeaderProfileSendTo", {},  function(request, response) {


    var leadername = request.slot("LEADER");
    var sendTo = request.slot("ESUBS");
    var session = request.getSession();

   
    initiateSession(request);
    session.set('mercer_leadername',leadername);
    //setSessionValues(this,leadername);

    if ((!sendTo && this.attributes['user'] !='') ||
    (sendTo && (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL"))) {
        sendTo =   session.get('user');
    }
    session.set('subs',sendTo);
    session.set('intent_to_exec','GetMeLeaderProfileSendTo');

    intent_GetConfirmation(request, response);
  });


app.listen(PORT);
console.log("Listening on port " + PORT + ", try http://localhost:" + PORT + "/alexaWS");