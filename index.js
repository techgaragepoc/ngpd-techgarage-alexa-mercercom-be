var Alexa = require('alexa-sdk');

const responses = require('./json/commonResponses.json');
const subscribers = require('./json/subscriberdetail.json');

const CommonMethods = require('./lib/commonmethods.js');
const MainService = require('./lib/mainservice.js');
const mailhandler = require('./awsmailhandler.js');


const APP_ID = 'amzn1.ask.skill.7b4f74d2-8aa3-46e8-9b3c-d93db755fbf1'; // old one 'amzn1.ask.skill.076a1fa2-cfed-4064-86c4-7c79e0006c7c';

var IsUserRecognized = false;
var AfterConfirmation = false;


function setSessionValues(obj,leadername)
{
        obj.attributes['mercer_leadername'] = leadername?leadername:((obj.attributes['mercer_leadername'] && obj.attributes['mercer_leadername'] != '')?obj.attributes['mercer_leadername']:'');
}

function initiateSession(obj)
 {

    var currUserId = obj.attributes['userId'];

    console.log('user id identified as => ' + obj.event.session.user.userId);
    console.log('current user => '+ currUserId);
    
    if (currUserId == null || currUserId == 'undefined' 
            || currUserId == '')
        {

            obj.attributes['user'] = '';
            obj.attributes['user_email'] = '';
            obj.attributes['mercer_leadername'] = '';
            obj.attributes['intent_to_exec'] = '';
            obj.attributes['info_subs'] = '';
            
            var user = CommonMethods.GetUserByUserId(obj.event.session.user.userId,subscribers);

            if (user) {
                obj.attributes['userId'] = user.alexaUserId;
                obj.attributes['user'] = user.name;
                obj.attributes['user_email'] = user.email;
                
                IsUserRecognized = true;
            }
        }
 }


 function responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext) {
    
        switch (responsetype.toUpperCase()) {
            case 'TELL': obj.emit(':tell',spkresponse); break;
            case 'ASK': obj.emit(':ask',spkresponse,prompt); break;
            case 'ASKWITHCARD': obj.emit(':askWithCard',spkresponse,prompt,cardtitle,cardtext); break;
        }
        
        return;     
     }


const handlers = {

    'LaunchRequest': function () {

     if (IsUserRecognized)
        {
            var welcomeMsg = 'Hi ' +  this.attributes['user']  + ', Welcome to Mercer Profile Service. how can I help you?';
            this.emit(':ask', welcomeMsg, welcomeMsg);
        }
        else {
            this.emit(':ask', responses["ProfileServiceStarted"].ask, responses["ProfileServiceStarted"].reprompt);
        }

        
    },
    'AMAZON.HelpIntent': function () {
        this.emit(':ask', responses["AMAZON.HelpIntent"].ask, responses["AMAZON.HelpIntent"].reprompt);
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', responses["AMAZON.StopIntent"].tell);
    },
    'AMAZON.CancelIntent': function () {
        this.emit('AMAZON.StopIntent');
    },
    'SessionEndedRequest': function () {
        this.emit('AMAZON.StopIntent');
    },
    'Unhandled': function () {
        this.emit('AMAZON.HelpIntent');
    },

    'NewSession001': function () {
      //  this.handler.state = '';
        this.attributes['user'] = '';
        this.attributes['user_email'] = '';
        this.attributes['mercer_leadername'] = '';
        this.attributes['info_subs']  = '';

        console.log(this.event.session.user.userId);
        var user = CommonMethods.GetUserByUserId(this.event.session.user.userId,subscribers);

        if (user) {
            this.attributes['userId'] = user.alexaUserId;
            this.attributes['user'] = user.name;
            this.attributes['user_email'] = user.email;
            
            IsUserRecognized = true;
        }

        this.emit('LaunchRequest');
    },
    'StartProfileService': function () {  //invoke statement: ask Mercer to start profile service

        this.emit(':ask', responses["ProfileServiceStarted"].ask, responses["ProfileServiceStarted"].reprompt);
    },
    'StopProfileService': function () {  //invoke statement: ask Mercer to stop profile service

        this.emit(':tell', responses["ProfileServiceStopped"].tell);
    },
    'IdentifyUser': function () {  //invoke statement: I am Arun

        var user = this.event.request.intent.slots.USER.value;
        var userEmailId = CommonMethods.GetEmailId(user,subscribers);

        if (user && userEmailId != '')
        {
            this.attributes['user'] = user;
            this.attributes['user_email'] = userEmailId;
            this.emit(':ask', 'Hi ' + user + ', how can I help you?', 'Hi ' + user + ', how can I help you?');
        }
        else {
            this.emit(':ask',"Sorry, I could not recognize you. Please try again <break time='2s'/>");
        }

    },

    'GetConfirmation' : function() {
        this.emit(':ask','I will send this information to ' + this.attributes['user_email'] + ', please confirm?' );
    },

    'SetConfirmation' : function() {
        var cnfrmResponse = this.event.request.intent.slots.CONSENT.value;
        var anotherAuthEmailId = this.event.request.intent.slots.AUTHEMAIL.value;

        if (cnfrmResponse.toUpperCase() == "YES" || cnfrmResponse.toUpperCase() == "YEP" 
                || cnfrmResponse.toUpperCase() == "YAA" || cnfrmResponse.toUpperCase() == "OK" )
        {
            console.log('mail confirmed to => ' + this.attributes['user_email']);
            AfterConfirmation = true;
            this.emit(this.attributes['intent_to_exec']);   
        }
        else {
            console.log('another email id => ' + anotherAuthEmailId);
            
            if (anotherAuthEmailId) {
                    this.attributes['user_email'] = anotherAuthEmailId;
                    this.emit('GetConfirmation');
            }
            else {
                AfterConfirmation = false;
                this.emit(':ask','ok');
            }
           
        }
    },


    'GetMeLeaderProfile': function () {
        var leadername = this.event.request.intent.slots.LEADER.value;

         this.attributes['mercer_leadername'] = leadername;

         initiateSession(this);
         setSessionValues(this,leadername);

         MainService.GetProfileSummary(this,leadername,mailhandler,null, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
            responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
        });
    },
    'GetMeLeaderProfileSendTo': function () {

         if (!AfterConfirmation) {

            var leadername = this.event.request.intent.slots.LEADER.value;
            var sendTo = this.event.request.intent.slots.ESUBS.value;

            this.attributes['mercer_leadername'] = leadername;

            initiateSession(this);
            setSessionValues(this,leadername);

            if ((!sendTo && this.attributes['user'] !='') ||
                (sendTo && (sendTo.toUpperCase() == "ME" || sendTo.toUpperCase() == "MY EMAIL"))) {
                sendTo =   this.attributes['user'];
                }

            this.attributes['info_subs'] = sendTo;
            this.attributes['intent_to_exec'] = 'GetMeLeaderProfileSendTo';

            this.emit('GetConfirmation');

         }
         else {

            var leadername = this.attributes['mercer_leadername'] ;
            var sendTo = this.attributes['info_subs'];
            
            AfterConfirmation = false;
            MainService.GetProfileSummary(this,leadername,mailhandler,sendTo, function(obj,responsetype,spkresponse,prompt,cardtitle,cardtext){
                responseToAlexa(obj,responsetype,spkresponse,prompt,cardtitle,cardtext);
            });
            
         }

         
    },

};

exports.handler = function (event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.appId = APP_ID;
    alexa.registerHandlers(handlers);
    console.log(event.session.user.userId);
    alexa.execute();
};
