const leadersprofile = require('../json/leaders.json');
const profileUrls = require('../json/profileURLs.json');
const subscribers = require('../json/subscriberdetail.json');

const ProfileService = require('./profileservice.js');
const CommonMethods = require('./commonmethods.js');


function SendResponse(obj,err,leadername,profiledata,mailhandler,subscriber,callback)
{

    if (profiledata)
        {
            var leadertitle = CommonMethods.GetTitle(profiledata.titles);

            if (subscriber)
            {
                var txtResponse = profiledata.profileHtml;
                var recipients = [];
                var subsemailid = CommonMethods.GetEmailId(subscriber,subscribers);
                var mailSubject = "Alexa Response: About " + leadername + " (" + leadertitle + ")";

                if (subsemailid == '')
                {
                    //obj.emit(":ask", "Sorry, i could not find subscriber email id <break time='3s'/>");
                    callback(obj,"ASK","Sorry, i could not find subscriber email id <break time='3s'/>","","","");
                }
                else {
                    recipients.push(subsemailid);
                    var mailContent = "Hi, \t\r\n \t\r\n As requested, profile summary - \t\r\n \t\r\n" + txtResponse;
                    mailhandler.SendMail(recipients,mailSubject,mailContent, function(err,data){

                        if (data) {
                            var spkResponse = "About " + leadername + " profile, " + data;
                            //obj.emit(":askWithCard", spkResponse, "Anything else you would like to ask", "Profile", txtResponse);
                            callback(obj,"ASKWITHCARD",spkResponse, "Anything else you would like to ask", "Profile", txtResponse);
                        }
                        else {
                            var spkResponse = "About " + leadername + " profile, error occured while sending mail to " + subsemailid + " - " + err;
                            //obj.emit(":askWithCard", spkResponse, "Anything else you would like to ask", "Profile", txtResponse);
                            callback(obj,"ASKWITHCARD",spkResponse, "Anything else you would like to ask", "Profile", txtResponse);
                        }
                    });
                }
            } 
            else {
                leadertitle = CommonMethods.ReplaceAll(leadertitle,"|","<break time='500ms'/>");
                var spkResponse = "About " + leadername + ", " + profiledata.profileText;
                console.log(spkResponse);
                //obj.emit(":askWithCard", spkResponse, "Anything else you would like to ask", "Profile", profiledata.profileHtml);
                callback(obj,"ASKWITHCARD",spkResponse, "Anything else you would like to ask", "Profile", profiledata.profileHtml)
            }
        }
        else
        {
           // obj.emit(":ask", "error occured while fetching the information, please try again <break time='3s'/>");
           callback(obj,"ASK","error occured while fetching the information, please try again <break time='3s'/>","","","");
        }
}

module.exports = {

GetProfileSummary: function(obj,leadername,mailhandler,subscriber, callback)
                        {
                            // leadername = self.attributes['mercer_leadername'];

                            console.log('leadername => ' + leadername);
                            if (!leadername || leadername == '')
                            {
                                //self.emit(":ask", "Sorry, i could not recognize leader's name");
                                callback(obj,"ASK","Sorry, i could not recognize leader's name","Sorry, i could not recognize leader's name","","");
                            }
                            else {

                                var leaderIdentity = ProfileService.GetLeaderIdentity(leadername,leadersprofile);
                                console.log(leaderIdentity);

                                 if (!leaderIdentity)
                                 {
                                    var msg = "Sorry, i could not find leader's detail of " + leadername; 
                                    //self.emit(":ask", msg);
                                    callback(obj,"ASK",msg,msg,"","");
                                 }
                                else {

                                        var leaderprofileId = leaderIdentity.profileid;
                                        var leaderOrganisation = leaderIdentity.organisation;

                                        ProfileService.GetLeaderProfile(leaderprofileId,leaderOrganisation,profileUrls,
                                                function(err, profiledata)  {

                                                    if (err) {
                                                        //cb(err,leadername,null,subscriber,self);
                                                        SendResponse(obj,err,leadername,null,mailhandler,subscriber,callback);
                                                    }

                                                    if (profiledata)
                                                     {
                                                        //cb(null,profiledata.name,profiledata,subscriber,self);
                                                        SendResponse(obj,null,profiledata.name,profiledata,mailhandler,subscriber,callback);
                                                     }
                                                });
                                    }
                                }
                        }

}