var CommonMethods = require('./commonmethods.js');
var ProfileHelper = require('./profilehelper.js');


module.exports = {

    GetLeaderIdentity: function (leadername,leadersprofile) {
                              var leaderIdentity = null;

                              for(var ldrCtr = 0;ldrCtr < leadersprofile.length; ldrCtr++)
                              {

                                if (CommonMethods.SearchStringInArray(leadername.toLowerCase(),leadersprofile[ldrCtr].names) > -1)
                                  {
                                     leaderIdentity = leadersprofile[ldrCtr];
                                     break;
                                  }
                              }
                              return leaderIdentity;
                            },

    GetLeaderProfile: function(leaderprofilename,organisation,profileUrls,callback)
                            {

                                var headers = {
                                        'content-type': 'application/json'
                                    };

                                var profileurlinfo = ProfileHelper.GetProfileUrlInfo(profileUrls,organisation);
                                var url = profileurlinfo.profiledata;
                                console.log(url);

                                if (profileurlinfo.type == "api") {
                                  url = url.replace('/#leaderprofileid/','/' + leaderprofilename + '/');

                                  ProfileHelper.GetInfoFromApi(url,headers,callback);
                                }
                                else {
                                  var filter = {profilename: leaderprofilename};  
                                  ProfileHelper.GetInfoFromHtml(url,headers,filter,callback);
                                }

                            }

}
