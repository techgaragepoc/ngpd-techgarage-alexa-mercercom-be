const request = require('request');
const CommonMethods = require('./commonmethods.js');
const striptags = require('striptags');
const cheerio = require('cheerio');



module.exports = {

GetProfileUrlInfo: function (profileUrls,organisation) {

                        for(var i=0;i<profileUrls.length;i++) {

                            if (organisation.toLowerCase() == profileUrls[i].organisation.toLowerCase())
                            return profileUrls[i];
                        }

                        return null;
                        },

 GetInfoFromApi: function(url,headers,cb) {
                        
                        var profile = {name:'',titles:[],profileText:'',profileHtml:''};
                        request.get({ url: url, headers: headers }, function(err, res, body) {
                                                      
                            if (!err) {                                                      
                                var profiledata = JSON.parse(body);

                                //var profile = {leadername: '',leadertitle:'', leaderprofilespk: '',leaderprofiletxt: ''};
                                var linebreak = '\t\r\n';

                                if (profiledata)
                                {
                                    //console.log(articles[i]);
                                    profile.name =  profiledata.leadername; // + "<break time='500ms'/>";
                                    profile.titles.push(profiledata.leadertitle);

                                    var profilebio = profiledata.leaderbio;
                                    profilebio =  CommonMethods.ReplaceAll(profilebio,'&nbsp;',' ');
                                    profilebio =  CommonMethods.ReplaceAll(profilebio,'</p>','');
                                    profilebio =  CommonMethods.ReplaceAll(profilebio,'<br>','');
                                    profilebio =  CommonMethods.ReplaceAll(profilebio,'&amp;','&');

                                    //sepcific to speak
                                    var profilespk =  striptags(profilebio,['<p>']);
                                    profilespk =  CommonMethods.ReplaceAll(profilespk,'<p>',"<break time='500ms'/>");
                                    profilespk =  CommonMethods.ReplaceAll(profilespk,'\n',"<break time='500ms'/>");
                                    profilespk =  CommonMethods.ReplaceAll(profilespk,'&',"and");

                                    //sepcific to html content
                                    var profilehtml =  CommonMethods.ReplaceAll(profilebio,'<p>',' ');
                                    profilehtml =  CommonMethods.ReplaceAll(profilehtml,'\n',linebreak);

                                    profile.profileHtml = profilehtml;
                                    profile.profileText = profilespk;

                                    // console.log(profile);
                                    cb(err,profile);
                                }
                                else 
                                    {
                                        console.log('error in profiledata');
                                        cb(new Error('error in response'),null); 
                                    }
                                
                           }
                          else {
                              console.log(err);
                              cb(err,null); 
                          }
                            
                      });
                        
                  },

GetInfoFromHtml: function(url,headers,filter,cb) {
                          var profile = {name:'',titles:[],profileText:'',profileHtml:''};
                          request.get({ url: url, headers: headers }, function(err, res, body) {
                                                     
                                                       if (!err) {
                                                        const $ = cheerio.load(body);

                                                        $('body').find('article').each(function(i, elem) {
                                                            var section = $(this).html();
                                                            var ldr = $(this).find('h1').text();
                                                            
                                                            if (ldr && ldr.toLowerCase() == filter.profilename.toLowerCase())
                                                              {
                                                                profile.name = $(this).find('h1').text();
                                                                profile.profileText = $(this).find('p').text();
                                                                profile.profileHtml = $(this).find('p').text();

                                                                var title = $(this).find('h2').text(); 
                                                                if ($(this).find('h3').text()){
                                                                    var othertitles = $(this).find('h3').text();
                                                                    othertitles = CommonMethods.ReplaceAll(othertitles,'\n',' ');
                                                                    othertitles = CommonMethods.ReplaceAll(othertitles,'\t','');
                                                                
                                                                    title += (' - ' + othertitles);
                                                                }

                                                                profile.profileText =  CommonMethods.ReplaceAll(profile.profileText,"&","and");

                                                                profile.titles.push(title);  
                                                              }
                                                               
                                                        });
                                                       
                                                        //console.log(x);
                                                       
                                                       // console.log(profile);
                                                        cb(err,profile);
                                                       }
                                                       else
                                                        cb(err,null); 
                                                    });
                    }

}
